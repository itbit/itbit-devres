itBit Merchant Example
======================

Setup
-----

The itBit merchant example is written in Python using the Bottle web framework. 

To setup the example store, first ensure that [Python 2.7](http://python.org/download/) is installed, along with the [PIP package manager](http://pypi.python.org/pypi/pip).

Assuming python and pip are both on the path, next install the required dependencies. At the command line:

> cd /path/to/merchant/test
> pip install -r requirements.txt

NOTE: Installing dependencies may require administrator privileges on your platform.

Before the example site can be run, it must be configured with an authorized itBit merchant key and secret, as well as the itBit OAuthv2 URI and merchant redirect URI. These values can be changed in config.py.

With the dependencies installed and configuration set, the example store can be run. Running the script requires two arguments: the binding address and port:

> python merchant.py 127.0.0.1 8080



Authorizing Payment
-------------------

The example merchant store presents two fictitious products for the customer to purchase. The two products are presented as checkboxes on a standard form (See listing.html).

When the form is submitted, the 'purchase' callback is invoked (merchant.py:21). The callback first calculates a total based on the selected items using a python Decimal type. Using this total, along with the merchants authentication key and redirect url, the store constructs an itBit authorization url and redirects the customer to this address. The BTC total to be transfered is encoded in the 'scope' parameter. In this example, the total is also used in the 'state' parameter for simplicity. In practice, the merchant should set this to an opaque transaction identifier.

NOTE: The given redirect url must exactly match the redirect url configured in the itBit user interface. If either the key or redirect url are not correct, the customer will receive a 400 response code.

At this point, the customer must login to itBit and authorize the requested amount. Assuming the customer has done so, the itBit site will redirect the customer to the given redirect url.

NOTE: At this point, the funds transfer has not occurred -- the customer has merely authorized that the merchant may initiate a transfer.

In the example application, the 'confirm' callback is used as the redirect target (merchant.py:60). First, the store checks the value of the 'error' query argurment. If it is present, the request is failed and the site displays the 'error' value to the user (merchant.py:62). In practice, the merchant application should gracefully handle these failures.

Next, the value of the 'state' parameter is parsed to determine the amount to charge (merchant.py:75). Once again, in practice this should be an opaque identifier.

NOTE: The state should not communicate the full transaction details. To guard against cross-site request forgery attacks, please use an opaque transaction identifier that ties the payment transaction to the specific purchasing transaction on your website, and validate that the same user that generated the payment authorization is being charged during the transfer. For guidance on cross site request forgery attacks, see http://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-10.12).

Before the transfer can take place, the merchant must first obtain an access token. The access token is obtained by presenting the value of the 'code' query arguement and the original redirect URI to the access token endpoint. The arguements are sent via an HTTP POST, with the arguments encoded according to the x-www-form-urlencoded MIME type (merchant.py:81). Additionally, the HTTP POST must include the merchant key and secret using the BASIC authentication method.

NOTE: Because BASIC authentication is being used, the calling code must ensure that the HTTPS endpoint is being used, and that the itBit certificate is authentic.

The access token endpoint will return a JSON response, containing either the access token or an error identifier. The merchant site must parse this result, failing gracefully if an error is encountered (merchant.py:87).

With the access token set, the merchant can initiate the transfer. Once again, an HTTP POST is issued, with the original transfer amount and currency as arugments. Additionally, the authorization token must be presented in the 'Authorization' HTTP header. If the transfer is successful, the details of the transfer will be displayed (confirm.html). Otherwise, an error field will be set on the response (merchant.py:101).



Deposits
--------

Support for deposits closely matches that for payments. The only difference comes in the way the 'scope' parameter is constructed. Note in the 'deposit' callback (merchant.py:42), the scope is created with the first parameter set to 'deposit'.


