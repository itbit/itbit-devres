from sys import argv
from os.path import join
from urllib import urlencode
from json import loads
from decimal import Decimal
from config import *

from bottle import run, get, post, static_file, request, redirect, abort
from bottle import mako_view as view
from requests.auth import HTTPBasicAuth
import requests


@get('/')
@view('listing.html') 
def root(): 
    return {} 


@post('/purchase')
def purchase():
    total = Decimal(0)
    if request.forms.widget1 == 'buy':
        total += Decimal("10.0")
    if request.forms.widget2 == 'buy':
        total += Decimal("20.0")

    scope = 'transfer:BTC:' + str(total)
    qargs = urlencode({
        'response_type': 'code',
        'client_id': AUTH_KEY,
        'redirect_uri': REDIRECT_URI,
        'scope': scope,
        'state': scope
    })

    url = '?'.join([AUTHORIZE_URI, qargs])
    redirect(url)


@post('/deposit')
def deposit():
    total = Decimal(10)

    scope = 'deposit:BTC:' + str(total)
    qargs = urlencode({
        'response_type': 'code',
        'client_id': AUTH_KEY,
        'redirect_uri': REDIRECT_URI,
        'scope': scope,
        'state': scope
    })

    url = '?'.join([AUTHORIZE_URI, qargs])
    redirect(url)


@get('/confirm')
@view('confirm.html')
def confirm():
    if request.query.error:
        abort(500, request.query.error)

    if not request.query.state:
        abort(500, "Invalid state")

    parts = request.query.state.split(':')

    if len(parts) != 3 or parts[0] not in ['transfer', 'deposit']:
        abort(500, "Invalid state")

    type, currency, amount = parts

    try:
        Decimal(amount)
    except ValueError:
        abort(500, "Invalid state")

    # Request the access token
    auth = HTTPBasicAuth(AUTH_KEY, AUTH_SECRET)
    resp = requests.post(ACCESS_TOKEN_URI, verify=False, auth=auth, data={
        'grant_type': 'authorization_code',
        'code': request.query.code,
        'redirect_uri': REDIRECT_URI })

    data = loads(resp.text)
    if 'error' in data:
        abort(500, data['error'])

    headers = {'Authorization': 'token ' + data['access_token']}
    data= {
        'currency': currency,
        'amount': amount
    }

    # Perform the transfer
    resp = requests.post(TRANSFER_URI, verify=False, data=data, headers=headers)

    data = loads(resp.text)

    if resp.status_code != 200:
        abort(resp.status_code, data['error'])

    return data


@get('/static/<filename:path>')
def static(filename):
    return static_file(filename, './static')


if __name__ == '__main__':
    run(host=argv[1], port=int(argv[2]))


