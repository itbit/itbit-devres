from json import loads
from pprint import pprint
from requests import get, post
from requests.auth import HTTPBasicAuth

TARGET_AUTH_KEY = 'mtRXkQw8DjPIc3X2s0iWs9hvfDZeYgq+tUhuJwumRvw='
TARGET_AUTH_SECRET = '8gkcy+sgiR2EfXAXqHSn+42r7/cQbF1wiUIFfB7S+L8='

SOURCE_AUTH_KEY = '4zI+pOnpfXBZEE79/j3E8JWA9UmkK8e4L8UYZ/5jrT0='
SOURCE_AUTH_SECRET = 'UyH/8/h8dY0Xzz6fu7Lc4trGez6QDBjLZdxj/oONRW4='

HOST = 'https://www.itbit.com/api'


def create_access_token(key, secret):
    auth = HTTPBasicAuth(key, secret)
    data = {'grant_type': 'client_credentials'}

    resp = post(HOST+'/token', verify=True, auth=auth, data=data)

    if resp.status_code != 200:
        print 'Invalid token response: %s' % resp.status_code
        if resp.text:
            print resp.text
        return

    data = loads(resp.text)
    return data['access_token']


def itbit_post(path, token, data=None):
    headers = {'Authorization': 'token '+token}
    if not data:
        headers['Content-Length'] = '0'

    resp = post(HOST+path, verify=True, data=data, headers=headers)

    if resp.status_code != 200:
        print 'Invalid response: %s' % resp.status_code
        if resp.text:
            print resp.text
        return

    return loads(resp.text)


def main():
    # First, create the access tokens for each account
    target_token = create_access_token(TARGET_AUTH_KEY, TARGET_AUTH_SECRET) 
    source_token = create_access_token(SOURCE_AUTH_KEY, SOURCE_AUTH_SECRET) 

    # Authorize the deposit into the target account
    payload = itbit_post('/accounts/Wallet/transfers/authorizations', target_token)
    print '-' * 50
    print 'Authorization payload'
    pprint(payload)

    # Perform the deposit with the authorization
    data = {
        'transfer_authorization': payload['authorization_code'],
        'amount': '10',
        'currency': 'BTC',
        'summary': 'Test Deposit'
    }
    payload = itbit_post('/accounts/Wallet/transfers/external', source_token, data=data)

    print '-' * 50
    print 'Deposit payload'
    pprint(payload)


if __name__ == '__main__':
    main()


