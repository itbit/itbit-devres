from json import loads
from pprint import pprint
from requests import get, post
from requests.auth import HTTPBasicAuth

AUTH_KEY = 'CuJV+NpoLPJy7dqM/7wdM1FG18BHZi3gDCpvi+XK8ls='
AUTH_SECRET = 'RN7ZrftE+4jxHHzHui0sR/eCfydCf0FzZ2dWOyl3bTQ='

HOST = 'https://www.itbit.com/api'

def main():
    # Generate the authorization token
    auth = HTTPBasicAuth(AUTH_KEY, AUTH_SECRET)
    data = {'grant_type': 'client_credentials'}

    resp = post(HOST+'/token', verify=True, auth=auth, data=data)

    if resp.status_code != 200:
        print 'Invalid token response: %s' % resp.status_code
        if resp.text:
            print resp.text
        return

    data = loads(resp.text)
    token = data['access_token']

    headers = {'Authorization': 'token '+token}

    resp = get(HOST+'/accounts/Wallet/transactions', verify=True, headers=headers)
    if resp.status_code != 200:
        print 'Invalid account transactions response: %s' % resp.status_code
        if resp.text:
            print resp.text
        return

    transactions = loads(resp.text)
    pprint(transactions)


if __name__ == '__main__':
    main()


